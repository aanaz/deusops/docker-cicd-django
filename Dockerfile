FROM python:3.9.16-slim

ENV PYTHONUNBUFFERED 1

ENV user admin
ENV email admin@example.com
ENV password pass

RUN apt update && apt install curl -y

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN mkdir db
COPY . .

EXPOSE 8000
VOLUME [ "/app/db" ]

HEALTHCHECK --interval=30s --timeout=30s --start-period=15s --retries=3 CMD [ "curl", "0.0.0.0:8000" ]

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000